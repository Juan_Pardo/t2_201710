package model.logic;

import model.data_structures.Lista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoSencillo;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private Lista<VOPelicula> misPeliculas;


	private Lista<VOAgnoPelicula> peliculasAgno;
	
	//METODOS 
	/**
	 * Carga de los archivos del  csv
	 */
	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		//Inicializo  las listas 
		misPeliculas= new ListaEncadenada<VOPelicula>();
		peliculasAgno=new ListaDobleEncadenada<VOAgnoPelicula>();
		ListaEncadenada<String> generos= new ListaEncadenada<String>();
		//Creo una lista  auxiliar para  los generos
		String  filename= "data/movies.csv";
		File file= new File(filename);
		String line= "";
		try {
			FileReader  fileR= new FileReader(file);
			BufferedReader br2 = new BufferedReader(fileR);
			br2.readLine();
			//Itero sobre cada linea
			while ((line= br2.readLine())!=null  ) {
				//Obtengo mi arreglo de  casillas
				String[] info=  line.split(",");

				//Primera posicion
				String id= info[0].replaceAll("\"", "");
				int idPelicula = Integer.parseInt(id);

				String nombrePelicula = null;
				//OBTENCION DEL NOMBRE
				//Caso ideal  donde  el arreglo solo tiene  3 posiciones
				if(info.length==3)
				{
					nombrePelicula= info[1].substring(0, info[1].length()-6);
				}
				//Caso donde el arreglo  es detamaÒo 4
				else if(info.length==4)
				{			
					String nombre=info[1] +","+ info[2];
					nombrePelicula=nombre;
				}
				//CASO SIZE=5
				else if(info.length==5)
				{
					String nombre=info[1]+"," + info[2]+ ", "+info[3];
					nombrePelicula=nombre;
				}

				//Caso donde  el arreglo es de  tamaÒo 6
				else if (info.length==6) {
					//obtengo el nombre conjunto
					String nombre=info[1]+ " , " + info[2]+ " , " + info[3]+ "," + info[4];
					nombrePelicula=nombre;
				}

				//CASO  SIZE=7
				else if (info.length==8) {
					//obtengo el nombre conjunto
					String nombre=info[1]+ " , " + info[2] + " , " + info[3]+ " , " + info[4]+ " , " + info[5];
					nombrePelicula=nombre;
				}			

				//Arreglo el nombre quitandole la fecha
				nombrePelicula.trim();
				nombrePelicula.replaceAll("\"", "");
				nombrePelicula.replace("-", "");
				nombrePelicula.substring(0, nombrePelicula.length()-7);


				//OBTENGO  EL A—O DEL ARCHIVO  PARA  CADA LINEA
				String infoDelAno= info[info.length-2];
				//Limpio todos los caracteres especiales del String
				infoDelAno.trim();
				infoDelAno= infoDelAno.replace("(", "");
				infoDelAno=infoDelAno.replace(")", "");
				infoDelAno=infoDelAno.replace("-", "");
				infoDelAno=infoDelAno.replace("\"", "");
				String aÒoPelicula = infoDelAno.substring(infoDelAno.length()-4 ,infoDelAno.length());
				int aÒo;
				try {
					aÒo= Integer.parseInt(aÒoPelicula);
				} catch (Exception e) {
					aÒo=0;
				}

				//OBTENGO  LOS GENEREOS DEL ARCHIVO PARA  CADA LINEA

				String  infoGeneros= info[info.length-1];
				String[] generosPelicula= infoGeneros.split("\\|");
				//Creo la lista de generos
				for (int i = 0; i < generosPelicula.length; i++) 
				{

					boolean  contenido=false;
					String infoCleaner=generosPelicula[i].replace("\"", "");

					if (generos.darNumeroElementos()==0) {
						generos.anexarElementoFinal(infoCleaner);
					}
					else {
						NodoSencillo<String> primer=generos.getFirst();
						while(primer!=null)
						{	
							String  value= primer.getItem();
							if (value.equals(infoCleaner)) {
								contenido=true;
							}
							primer=primer.getNext();
						}
					}
					if (contenido==false) {
						generos.anexarElementoFinal(infoCleaner);
					}
					generos.anexarElementoFinal(infoCleaner);
				}
				//Creo la pelicula
				VOPelicula nueva= new  VOPelicula();
				nueva.setTitulo(nombrePelicula);
				nueva.setAgnoPublicacion(aÒo);
				nueva.setGenerosAsociados(generos);
				//Lo agrego a la  lista del SR
				misPeliculas.anexarElementoFinal(nueva);
			}
	} catch (Exception e) {
		e.printStackTrace();
	}
}



@Override
public Lista<VOPelicula> darListaPeliculas(String busqueda) 
{
	Lista<VOPelicula> nuevaLista = new ListaEncadenada<>();

	for (VOPelicula pelicula : misPeliculas)
	{
		if(pelicula.getTitulo().contains(busqueda)) {
			nuevaLista.anexarElementoFinal(pelicula);
		}
	}
	return nuevaLista;
}

@Override
public Lista<VOPelicula> darPeliculasAgno(int agno) 
{
	Lista<VOPelicula> nuevaLista = new ListaEncadenada<>();

	for (VOPelicula pelicula : misPeliculas)
	{
		if(pelicula.getAgnoPublicacion()==agno) {
			nuevaLista.anexarElementoFinal(pelicula);
		}
	}
	return nuevaLista;
}

@Override
public VOAgnoPelicula darPeliculasAgnoSiguiente() {
	VOAgnoPelicula peliculasAgnos = (VOAgnoPelicula) darPeliculasAgno((peliculasAgno.darElemento(0).getAgno()) + 1);
	return peliculasAgnos;
}

@Override
public VOAgnoPelicula darPeliculasAgnoAnterior() {
	VOAgnoPelicula peliculasAgnos = (VOAgnoPelicula) darPeliculasAgno((peliculasAgno.darElemento(0).getAgno()) - 1);
	return peliculasAgnos;
}

}

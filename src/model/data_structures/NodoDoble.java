package model.data_structures;

public class NodoDoble<T> 
{

	//ATRIBUTOS
	
	/**
	 * Valor guardado dentro del nodo
	 */
	private T item;
	
	/**
	 * Referencia  al siguiente  nodo
	 */
	private  NodoDoble<T> siguiente;
	
	/**
	 * Referencia al anterior nodo
	 */
	private NodoDoble<T> anterior;
	
	
	//CONSTRUCTOR
	public  NodoDoble (T pItem )
	{
		item=pItem;
		siguiente=null;
		anterior=null;
	}
	
	
	//METODOS
	/**
	 * Obtiene el valor del elemento guardado en el nodo
	 * @return item del nodo
	 */
	public  T getItem(){
		return item;
	}
	
	/**
	 * Obtiene el nodo concatenado al nodo actual
	 * @return el siguiente nodo 
	 */
	public  NodoDoble<T> getNext(){
		if (siguiente!=null)return siguiente;
		return null;
	}
	
	/**
	 * Retorna el anterior
	 * @return anterior al nodo
	 */
	public NodoDoble<T> getPrevious()
	{
		return anterior;
	}
	
	/**
	 * Agrega un siguiente a  un nodo
	 * @param next Nodo de tipo T 
	 */
	public void setNext(NodoDoble<T> next)
	{
		siguiente=next;	
	}
	
	/**
	 *Agrega un anterior al nodo 
	 */
	public void setPrevious(NodoDoble<T> previous)
	{
		anterior=previous;
	}
	
	
	/**
	 * Borra el la referencia al siguiente nodo
	 */
	public void EliminateNext()
	{
		siguiente=null;
	}
	
	/**
	 * Eliminar  el anterior
	 */
	public  void  EliminatePrevious()
	{
		anterior=null;
	}
}

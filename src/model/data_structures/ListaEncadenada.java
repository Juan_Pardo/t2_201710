package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements Lista<T> {

	//ATRIBUTOS
	
	/**
	 * Primer Elemento
	 */
	private NodoSencillo<T> first;
	
	/**
	 *Nodo actual 
	 */
	private  NodoSencillo<T> actual;
	
	/**
	 * Ultimo Elemento
	 */
	private  NodoSencillo<T> last;
	
	/**
	 * Tama�o
	 */
	private int  size;
	
	//CONSTRUCTOR
	public  ListaEncadenada() 
	{
		first=null;
		size=0;
		last=null;
	}
	
	//ATRIBUTOS
	@Override
	public Iterator<T> iterator() {
		return this.iterator();
	}

	@Override
	public void anexarElementoFinal(T elem) {
		NodoSencillo<T> newNode=new NodoSencillo<T>(elem);
		last.setNext(newNode);
		last=newNode;
		size++;
		if(size==0)
		{
			first=newNode;
			last=first;
			size++;
		}
		else
		{
			last.setNext(newNode);
			last = newNode;
			size++;
		}
	}

	@Override
	public T darElemento(int pos) 
	{
			T resp=null;
			int contador=1;
			NodoSencillo<T> actual= first;
			while(actual!=null)
			{
				T obj= actual.getItem();
				if (contador==pos) {
					resp=obj;
				}
				actual=actual.getNext();
			}
			return resp;
	}
	
	public void deleteall()
	{
		size=0;
		first=null;
		last=null;
	}
	
	@Override
	public int darNumeroElementos() {
		return size;
	}
	
	public  NodoSencillo<T> getLast()
	{
		return last;
	}
	
	@Override
	public T darElementoPosicionActual() {
		return actual.getItem();
	}

	@Override
	public void avanzarSiguientePosicion() {
	  actual.getNext();
		
	}

	@Override
	public void retrocederPosicionAnterior() {
	}

	public NodoSencillo<T> getFirst() {
		return first;
	}
}

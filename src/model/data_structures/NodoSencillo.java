package model.data_structures;

public class NodoSencillo<T>
{
	//ATRIBUTOS
	
		/**
		 * Valor guardado dentro del nodo
		 */
		private T item;
		
		/**
		 * Referencia  al siguiente  nodo
		 */
		private  NodoSencillo<T> siguiente;
			
		
		//CONSTRUCTOR
		public  NodoSencillo (T pItem )
		{
			item=pItem;
			siguiente=null;
		}
		
		//METODOS
		/**
		 * Obtiene el valor del elemento guardado en el nodo
		 * @return item del nodo
		 */
		public  T getItem(){
			return item;
		}
		
		/**
		 * Obtiene el nodo concatenado al nodo actual
		 * @return el siguiente nodo 
		 */
		public  NodoSencillo<T> getNext(){
			if (siguiente!=null)return siguiente;
			return null;
		}
		
		/**
		 * Agrega un siguiente a  un nodo
		 * @param next Nodo de tipo T 
		 */
		public void setNext(NodoSencillo<T> next)
		{
			siguiente=next;	
		}
		
		/**
		 * Borra el la referencia al siguiente nodo
		 */
		public void EliminateNext()
		{
			siguiente=null;
		}
}

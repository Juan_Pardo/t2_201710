package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements Lista<T> {

	
	//ATRIBUTOS
	 
	private NodoDoble<T> primero;
	
	private NodoDoble<T> ultimo;
	
	private NodoDoble<T> actual;
	
	private int size;
	
	//CONSTRUCTOR
	public ListaDobleEncadenada()
	{
		primero=null;
		ultimo=null;
		size=0;
	}

	//METODOS
	@Override
	public Iterator<T> iterator() {
		return this.iterator();//TODO posible
	}

	@Override
	public void anexarElementoFinal(T elem) {
		NodoDoble<T> newNode= new NodoDoble<T>(elem);
		ultimo.setNext(newNode);
		newNode.setPrevious(ultimo);
		ultimo = newNode;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		
		T resp=null;
		int contador=1;
		NodoDoble<T> actual= primero;
		while(actual!=null)
		{
			T obj= actual.getItem();
			if (contador==pos) {
				resp=obj;
			}
			actual=actual.getNext();
		}
		return resp;
	}


	@Override
	public int darNumeroElementos() {
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		return actual.getItem();
	}

	@Override
	public void avanzarSiguientePosicion() {
		actual=actual.getNext();
		
	}

	@Override
	public void retrocederPosicionAnterior() {
		actual=actual.getPrevious();
		
	}


}

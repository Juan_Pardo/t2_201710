package api;

import model.data_structures.Lista;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

public interface IManejadorPeliculas {
	
	public void cargarArchivoPeliculas(String archivoPeliculas);
	
	public Lista<VOPelicula> darListaPeliculas(String busqueda);
	
	public Lista<VOPelicula> darPeliculasAgno(int agno);

	public VOAgnoPelicula darPeliculasAgnoSiguiente();

	public VOAgnoPelicula darPeliculasAgnoAnterior();
}
